# Spring WebFlux Essentials

![WebFlux](webflux.jpg)

Fundamentos de estudo do Spring WebFlux. <br>
Projeto base de estudo: https://github.com/devdojoacademy/spring-webflux-essentials

## R2DBC
O projeto Reactive Relational Database Connectivity ([R2DBC](https://r2dbc.io/)) conduz APIs de programação reativa para bancos de dados relacionais.
Não provê suporte para JPA.

 - Com base na especificação Reactive Streams.
 - Trabalha com bancos de dados relacionais.
 - Oferece suporte a soluções escaláveis. Com o Reactive Streams, o R2DBC permite que você mude do modelo clássico de “um thread por conexão” para uma abordagem mais poderosa e escalável.
 - Fornece uma especificação aberta.


## Spring Webflux
[Documentação Spring Webflux.](https://docs.spring.io/spring-framework/docs/current/reference/html/web-reactive.html)
O Spring Webflux é baseado no projeto Reactor, que é uma biblioteca de programação reativa sem bloqueio para a JVM, mas é interoperável com outras bibliotecas reativas por meio do Reactive Streams.
Webflux é não-bloqueante e suporta contrapressão.

Assim como o Reactor, ele possui dois tipos: `Flux` e `Mono`. Na programação reativa trabalha-se com fluxos e não com dados. <br> 

Como regra geral, uma API WebFlux aceita um simples Publisher como entrada, adapta-o a um tipo Reactor internamente, usa-o e retorna a Flux ou a Mono como saída.

 - O tipo *Flux* consiste em um fluxo (stream) de 0 a N elementos.
 - O tipo *Mono* consiste em um fluxo (stream) de 0 ou 1 elemento apenas.

A classe `org.springframework.http.server.reactive.ServletHttpHandlerAdapter` é responsável por realizar o subscribe no método `service`.

#### Defina "Reativo"
O termo “reativo” refere-se a modelos de programação que são construídos em torno da reação à mudança - componentes de rede reagindo a eventos de I / O, controladores de IU reagindo a eventos de mouse e outros.
Nesse sentido, o não bloqueio é reativo, porque, em vez de ser bloqueado, estamos agora no modo de reagir às notificações à medida que as operações são concluídas ou os dados se tornam disponíveis.

Outro mecanismo importante que o Spring associa como "reativo" é a `contrapressão` sem bloqueio. No código síncrono e imperativo, o bloqueio de chamadas serve como uma forma natural de contrapressão que força o chamador a esperar. Em código sem bloqueio, torna-se importante controlar a taxa de eventos para que um produtor rápido não sobrecarregue seu destino.

![Exemplo de processamento em aplicação não bloqueante](exemplo_processamento.jpg)
Exemplo de processamento em aplicação não bloqueante

Leitura: [Spring Webflux Medium](https://medium.com/@michellibrito/spring-webflux-f611c8256c53)


### Subscribing/Inscrevendo-se em um Stream
O método subscribe() coleta todos os elementos de um stream.
```java
    List<Integer> elements = new ArrayList<>();
    
    Flux.just(1, 2, 3, 4)
    .log()
    .subscribe(elements::add);
```
Podemos observar o log:
```shell
20:25:19.550 [main] INFO  reactor.Flux.Array.1 - | onSubscribe([Synchronous Fuseable] FluxArray.ArraySubscription)
20:25:19.553 [main] INFO  reactor.Flux.Array.1 - | request(unbounded)
20:25:19.553 [main] INFO  reactor.Flux.Array.1 - | onNext(1)
20:25:19.553 [main] INFO  reactor.Flux.Array.1 - | onNext(2)
20:25:19.553 [main] INFO  reactor.Flux.Array.1 - | onNext(3)
20:25:19.553 [main] INFO  reactor.Flux.Array.1 - | onNext(4)
20:25:19.553 [main] INFO  reactor.Flux.Array.1 - | onComplete()
```

Sequência de registros:
 - onSubscribe() - É chamado quando nos inscrevemos em nosso stream.
 - request(unbounded) - Quando chamamos subscribe, nos bastidores estamos criando uma Subscription. Esta assinatura solicita elementos do fluxo. Neste caso, o padrão é ilimitado (unbounded), o que significa que solicita cada elemento disponível.
 - onNext() - É chamado em cada elemento.
 - onComplete() - É chamado por último, após receber o último elemento.
Há um onError() também, que seria chamado se houvesse uma exceção, mas, neste caso não há.

### Tratamento de erros/exceções

#### AbstractErrorWebExceptionHandler
Segundo [documentação](https://docs.spring.io/spring-boot/docs/current-SNAPSHOT/reference/htmlsingle/#web.reactive.webflux.error-handling).
Para lidar com erros de WebFlux em um nível global precisamos realizar apenas duas etapas:
 1. Personalize os Atributos de Resposta de Erro Global
    Estendendo a classe `DefaultErrorAttributes` e substituindo seu método getErrorAttributes() é possível personalizar o status HTTP e um corpo de erro JSON que o  que manipulador criado lançará.
 2. Implementar o manipulador de erros global
    Para isso, o Spring fornece uma classe `AbstractErrorWebExceptionHandler` conveniente para estendermos e implementarmos no tratamento de erros globais.<br>
    A classe `GlobalExceptionHandler` possui ordem -2. Isso é para dar a ele uma prioridade mais alta do que `DefaultErrorWebExceptionHandler` que está registrado em @Order (-1).
    ```java
        @Component
        @Order(-2)
        public class GlobalExceptionHandler extends AbstractErrorWebExceptionHandler {
    ```
    
Com esta implementação é possível identificar a exceção e customizar o corpo do objeto retornado na resposta.
```java
@Component
public class GlobalErrorAttributes extends DefaultErrorAttributes {
    
    @Override
    public Mono<Void> handle(ServerWebExchange exchange, Throwable ex) {
        if (throwable instanceof ResponseStatusException) {
            // Tratar pos tipo de Exception
        }
```

Contudo não é possível alterar o status HTTP da resposta através desta abordagem.

#### ControllerAdvice
Segundo [documentação](https://docs.spring.io/spring-framework/docs/current/reference/html/web-reactive.html#webflux-dispatcher-exceptions) do Spring  "tenha em mente que, no WebFlux, você não pode usar um `@ControllerAdvice` para manipular exceções que ocorrem antes de um manipulador ser escolhido." <br> 

Foi utilizada a anotação `@RestControllerAdvice` que é uma anotação de conveniência que também é anotada com @ControllerAdvice e @ResponseBody.

A utilização do ControllerAdvice exemplificada na [documentação Spring](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#features.developing-web-applications.spring-mvc.error-handling) 
estende de `ResponseEntityExceptionHandler`, que faz parte do pacote `org.springframework.web.servlet.mvc` (spring-boot-starter-web).
Como esse artefato não foi adicionado ao projeto não estamos utilizando essa implementação.

##### Análises de concorrência de manipuladores:

 - Com classes estendendo `AbstractErrorWebExceptionHandler`, `DefaultErrorAttributes` e outra anotada com `@ControllerAdvice/@RestControllerAdvice` coexistindo no projeto, foi possível observar que a implementação em ControllerAdvice é executada e as demais são ignoradas, seus métodos não são chamados.
   Adicionando classe que implementa `ErrorWebExceptionHandler` no cenário anterior, foi observado que esta também é ignorada, em caso de exceção o método em ControllerAdvice é acionado.


 - Com classes estendendo `AbstractErrorWebExceptionHandler` e `DefaultErrorAttributes`, e outra implementando `ErrorWebExceptionHandler`, a classe que implementa *ErrorWebExceptionHandler* é a que é acionada,
os manipuladores nas demais são ignoradas.


 - Não havendo no projeto @ControllerAdvice e não havendo implementação de `ErrorWebExceptionHandler`, a classe que estende `AbstractErrorWebExceptionHandler` fica responsável pela manipulação das exceptions, no que tange à construção do corpo de resposta.


 - Não havendo estensão de `DefaultErrorAttributes` e `AbstractErrorWebExceptionHandler`, nem implementação de `ErrorWebExceptionHandler` a classe anotada com *ControllerAdvice* fica responsável por manipular exceções.

#### Leituras
 - [Spring Docs: Tratamento de Erros](https://docs.spring.io/spring-boot/docs/2.1.x/reference/html/boot-features-developing-web-applications.html#boot-features-webflux-error-handling)
 - [Handling Errors in Spring WebFlux](https://www.baeldung.com/spring-webflux-errors).


### Testes
Em um teste de integração idelmente haveria um banco de dados de teste.

#### @WebFluxTest
Anotação que pode ser usada para um teste Spring WebFlux que se concentra apenas em componentes Spring WebFlux.
O uso dessa anotação desabilitará a configuração automática completa e, em vez disso, aplicará apenas a configuração relevante aos testes WebFlux
(ou seja, @Controller, @ControllerAdvice, @JsonComponent, Converter/GenericConverter e os beans WebFluxConfigurer, mas não os beans @Component, @Service ou @Repository).

Inicializa o conteúdo relacionado ao WebWebFlux, porém não scaneia todo o pacote, daí a utilização de `@Import(Myclass.class)`.

#### WebTestClient e @WithUserDetails
Para teste de integração do controller foi injetada a dependência `org.springframework.test.web.reactive.server.WebTestClient` que fornece um client para realizar solicitações e verificar as respostas.
Segundo [documentação](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/test/web/reactive/server/WebTestClient.html) "Este cliente pode se conectar a qualquer servidor por HTTP ou a um aplicativo WebFlux por meio de objetos simulados de solicitação e resposta." e não pode ser utilizado no Kotlin (2021).

Além disso, foi utilizada a anotação `@WithUserDetails` adicionada aos métodos de teste para emular a requisição com a authenticação de um usuário retornado do UserDetailsService implementado, no nosso caso `ReactiveUserDetailsService`.
O método realizará a autenticação real do usuário verificando os dados no banco de dados, para mockar esse comportamento pode-se mockar a consulta *findByUsername* no cenário proposto aqui.

### Níveis de Log

Leitura: [Setting the Log Level in Spring Boot when Testing](https://www.baeldung.com/spring-boot-testing-log-level)

### BlockHound
O BlockHound é um agente Java que instrumentará de forma transparente as classes JVM e interceptará as chamadas de bloqueio (por exemplo, IO) se forem realizadas a partir de threads marcados como "somente operações sem bloqueio" (ou seja, threads que implementam a NonBlockinginterface do marcador do Reactor , como aquelas iniciadas por Schedulers.parallel()).
Se e quando isso acontecer (mas lembre-se, isso nunca deve acontecer! 😜), um erro será gerado.

A programação reativa é baseada na passagem assíncrona de mensagens. Uma cadeia de chamadas geralmente envolve várias etapas,
onde cada etapa, chamada de processador, atua como assinante da próxima e como produtor da anterior.
Se uma chamada da cadeia estiver bloqueando, ela "congela" toda a cadeia e diminui os benefícios da abordagem reativa.

```java
    BlockHound.install(builder -> {
        // Marcando thread 'main' como não bloqueante
        builder.nonBlockingThreadPredicate(current ->
        current.or(thread -> thread.getName().equals("main")));
        // Permitindo bloqueio em método
        builder.allowBlockingCallsInside("java.util.ResourceBundle", "loadBundle");
    });
```

Leitura:
 - [BlockHound: como funciona](https://blog.frankel.ch/blockhound-how-it-works/)
 - [BlockHound - git](https://github.com/reactor/BlockHound)

### Documentação

Para habilitar documentação OpenApi foi adicionada a dependência:

```xml
<dependency>
    <groupId>org.springdoc</groupId>
    <artifactId>springdoc-openapi-webflux-ui</artifactId>
</dependency>
```

Para permitir o acesso à página "http://localhost:8080/swagger-ui.html" foi adicionada a permissão na classe `SecurityConfig`: 
`.pathMatchers("/webjars/**", "/v3/api-docs/**", "/swagger-ui.html").permitAll()`.

Para evitar exceção "Blocking Call" pelo BlockHound foi necessário habilitar os seguintes métodos em `SpringWebfluxEssentialsApplication`:
``` java
    builder.allowBlockingCallsInside("java.io.InputStream", "readNBytes");
    builder.allowBlockingCallsInside("java.io.FilterInputStream", "read");`
```

Para exibir o formulário de login na página de documentação e replicar o comportamento de segurança de roles de usuário foi adicionada a anotação a seguir no controller:
```java
@SecurityScheme(name = "Basic Authentication",
        type = SecuritySchemeType.HTTP,
        scheme = "basic")
```

E para habilitar o login específico para cada método pode ser adicionada a seguinte anotação:
```java
    @Operation(summary = "Descrição da requisição",
            security = @SecurityRequirement(name = "Basic Authentication"),
            tags = {"anime"})
```

----
### Outros tópicos

#### Database MySql
`CREATE DATABASE webflux`

#### Lombok
##### With
[@With](https://projectlombok.org/features/With) fornece um setter para uma propriedade imutável, construindo um clone do objeto com um novo valor para a referida propriedade.

#### Issue

 - Para evitar o erro [OpenJDK 13 requires -XX:+AllowRedefinitionToAddDeleteMethods flag](https://github.com/reactor/BlockHound/issues/33) deve ser adicionada a referida flag, a imagem descreve o fluxo no Intellij:
![Erro flag Blockhound](erro_AllowRedefinitionToAddDeleteMethods.png)
   
Além de adicionar no pom.xml:
```xml
	<plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>2.22.2</version>
        <configuration>
            <argLine>-XX:+AllowRedefinitionToAddDeleteMethods</argLine>
        </configuration>
    </plugin>
```

#### Developer
[Kamila Serpa - GitHub Page](https://kamilaserpa.github.io)

[1]: https://www.linkedin.com/in/kamila-serpa/

---
[![linkedin]("https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white")][1]

---
