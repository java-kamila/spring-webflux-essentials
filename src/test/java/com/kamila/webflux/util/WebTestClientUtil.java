package com.kamila.webflux.util;

import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers;
import org.springframework.stereotype.Component;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.client.ExchangeFilterFunctions;

/**
 * Classe criada para auxiliar na construção de usuários com permissões diferentes
 * para realização de testes.
 *  webTestclientUtil.authenticateClient("kamila", "webflux");
 *  Com a utilização da anotação @WithUserDetails no teste de integração, está não foi mais utilizada.
 */
@Component
@RequiredArgsConstructor
public class WebTestClientUtil {

    private final ApplicationContext applicationContext;

    // Retorna WebTestClient a partir do username e password
    public WebTestClient authenticateClient(String username, String password) {
        return WebTestClient.bindToApplicationContext(applicationContext) // servidor, porta automaticamente carregados
                .apply(SecurityMockServerConfigurers.springSecurity())
                .configureClient()
                // Sempre que fizer exchanges é feita essa autenticação básica
                .filter(ExchangeFilterFunctions.basicAuthentication(username, password))
                .build();
    }

}
