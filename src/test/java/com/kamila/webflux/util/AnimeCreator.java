package com.kamila.webflux.util;

import com.kamila.webflux.domain.Anime;

/**
 * Classe builder de Anime
 */
public class AnimeCreator {

	public static Anime createAnimeToBeSaved() {
		return Anime.builder()
				.name("Pokemon")
				.build();
	}
	
	public static Anime createValidAnime() {
		return Anime.builder()
				.id(1L)
				.name("Pokemon")
				.build();
	}
	
	public static Anime createValidUpdateAnime() {
		return Anime.builder()
				.id(1L)
				.name("Pokemon 2")
				.build();
	}
	
}
