create table tb_user (
	id bigint not null auto_increment,
	name varchar(255) not null,
	username varchar(100) not null,
	password varchar(150),
	authorities varchar(150),
	primary key(id)
) engine=InnoDB default charset=utf8;
