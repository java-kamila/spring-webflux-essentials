-- Desabilita checagem de foreign keys para permitir a deleção dos dados de todas as tabelas a seguir
set foreign_key_checks = 0;

delete from tb_anime;
delete from tb_user;

set foreign_key_checks = 1;

INSERT INTO tb_anime (id, name) VALUES (1, 'Dragon Ball Z');
INSERT INTO tb_anime (id, name) VALUES (2, 'Yu Yu Hakusho');
INSERT INTO tb_anime (id, name) VALUES (3, 'Pokémon');
INSERT INTO tb_anime (id, name) VALUES (4, 'Yu-Gi-Oh!');
INSERT INTO tb_anime (id, name) VALUES (5, 'Saint Seiya');

INSERT INTO tb_user (id, name, username, password, authorities) VALUES
(1, "Kamila Serpa", "kamila", "{bcrypt}$2a$10$v38Tf6Lx4gYlfEZfvQD0sOBBOi0hdL.6BELfwvxGcudD3a8Si6g3O", "ROLE_ADMIN,ROLE_USER");
INSERT INTO tb_user (id, name, username, password, authorities) VALUES
(2, "Peter Parker", "peter", "{bcrypt}$2a$10$v38Tf6Lx4gYlfEZfvQD0sOBBOi0hdL.6BELfwvxGcudD3a8Si6g3O", "ROLE_USER");