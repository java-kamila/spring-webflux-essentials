package com.kamila.webflux;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import reactor.blockhound.BlockHound;

@SpringBootApplication
public class SpringWebfluxEssentialsApplication {

	static {
		// Certificando-se de que não estamos bloqueando nenhuma thread
		BlockHound.install(builder -> {
			builder.allowBlockingCallsInside("java.util.UUID", "randomUUID");

			// Evitando "Blocking call" em MessageSource.getMessage
			builder.allowBlockingCallsInside("java.util.ResourceBundle", "loadBundle");

			// Evitando "Blocking call" no Swagger/OpenApi
			builder.allowBlockingCallsInside("java.io.InputStream", "readNBytes");

			builder.allowBlockingCallsInside("java.io.FilterInputStream", "read");
		});
	}
	
	public static void main(String[] args) {
//		Capturando senha "webflux" encriptada para persistir no banco
//		System.out.println(PasswordEncoderFactories.createDelegatingPasswordEncoder().encode("webflux"));
		SpringApplication.run(SpringWebfluxEssentialsApplication.class, args);
	}

}
