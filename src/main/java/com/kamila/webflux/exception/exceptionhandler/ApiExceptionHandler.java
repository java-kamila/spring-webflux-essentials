package com.kamila.webflux.exception.exceptionhandler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kamila.webflux.exception.AnimeNaoEncontradoException;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.reactive.error.ErrorWebExceptionHandler;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.io.Serializable;
import java.util.stream.Collectors;

import static reactor.core.publisher.Mono.just;

/**
 * Seus métodos são acionados se não houver implementação de @ControllerAdvice no projeto.
 */
//public class ApiExceptionHandler {}
/* Habilitando GlobalExceptionHandler
@Slf4j
@Configuration
@Order(-2)
public class ApiExceptionHandler implements ErrorWebExceptionHandler {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    MessageSource messageSource;

    @SneakyThrows
    @Override
    public Mono<Void> handle(ServerWebExchange exchange, Throwable ex) {

        log.info(">> ApiExceptionHandler <<");
        log.error("[EXCEPTION]: " + ex.getMessage());

        exchange.getResponse().getHeaders().setContentType(MediaType.APPLICATION_JSON);

        // Tratando exceções específicas
        if (ex instanceof WebExchangeBindException) {
            String problemObjects = ((WebExchangeBindException) ex).getAllErrors().stream()
                    .map(objectError -> {
                        String message = messageSource.getMessage(objectError,
                                LocaleContextHolder.getLocale());

                        String name = objectError.getObjectName();

                        if (objectError instanceof FieldError) {
                            name = ((FieldError) objectError).getField();
                        }
                        return name + ": " + message;
                    })
                    .collect(Collectors.joining("."));

            exchange.getResponse().setStatusCode(HttpStatus.BAD_REQUEST);
            Problem problem = ExceptionUtils.createProblem(HttpStatus.BAD_REQUEST,
                    exchange,
                    problemObjects,
                    ex.getMessage())
                    .build();

            return handleException(exchange, problem);
        }

        if (ex instanceof AnimeNaoEncontradoException) {
            exchange.getResponse().setStatusCode(HttpStatus.NOT_FOUND);
            Problem problem = ExceptionUtils.createProblem(HttpStatus.NOT_FOUND,
                    exchange,
                    ProblemMessages.RECURSO_NAO_ENCONTRADO.getMessage(),
                    ex.getMessage())
                    .build();

            return handleException(exchange, problem);
        }

        // Extraindo httpStatusCode da Exception
        HttpStatus httpStatus = ExceptionUtils.getHttpStatusFromExceptionMessage(ex.getMessage());
        // Problem Default
        Problem problem = ExceptionUtils.createProblem(httpStatus,
                exchange,
                ProblemMessages.MSG_ERRO_GENERICA.getMessage(),
                ex.getMessage())
                .build();
        exchange.getResponse().setStatusCode(httpStatus);

        return handleException(exchange, problem);
    }


    private Mono<Void> handleException(ServerWebExchange exchange, Serializable problem) throws JsonProcessingException {
        try {
            var buffer = exchange.getResponse().bufferFactory()
                    .wrap(objectMapper.writeValueAsBytes(problem));

            return exchange.getResponse().writeWith(just(buffer));

        } catch (Exception e) {
            Problem problemException = ExceptionUtils.createProblem(HttpStatus.INTERNAL_SERVER_ERROR, exchange,
                    ProblemMessages.MSG_ERRO_GENERICA.getMessage(),
                    e.getMessage())
                    .build();

            var exceptionBuffer = exchange
                    .getResponse()
                    .bufferFactory()
                    .wrap(objectMapper.writeValueAsBytes(problemException));

            return exchange.getResponse().writeWith(just(exceptionBuffer));
        }
    }

}
*/