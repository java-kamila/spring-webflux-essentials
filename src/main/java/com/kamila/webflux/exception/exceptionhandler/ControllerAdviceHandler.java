package com.kamila.webflux.exception.exceptionhandler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kamila.webflux.exception.AnimeNaoEncontradoException;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.io.Serializable;
import java.util.stream.Collectors;

import static reactor.core.publisher.Mono.just;

@Slf4j
//@ControllerAdvice
public class ControllerAdviceHandler {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    MessageSource messageSource;

    @SneakyThrows
    @ExceptionHandler(Throwable.class)
    public Mono<Void> handle(ServerWebExchange exchange, Throwable ex) {

        log.info(">> ControllerAdviceHandler - handle <<");
        log.error("[EXCEPTION]: " + ex.getMessage());

        exchange.getResponse().getHeaders().setContentType(MediaType.APPLICATION_JSON);
        var factory = exchange.getResponse().bufferFactory();

        // Tratando exceções específicas
        if (ex instanceof WebExchangeBindException) {
            log.warn(">> CtrlAdv - WebExchangeBindException");

            String problemObjects = ((WebExchangeBindException) ex).getAllErrors().stream()
                    .map(objectError -> {
                        String message = messageSource.getMessage(objectError,
                                LocaleContextHolder.getLocale());

                        String name = objectError.getObjectName();

                        if (objectError instanceof FieldError) {
                            name = ((FieldError) objectError).getField();
                        }
                        return name + ": " + message;
                    })
                    .collect(Collectors.joining("."));

            exchange.getResponse().setStatusCode(HttpStatus.BAD_REQUEST);
            Problem problem = ExceptionUtils.createProblem(HttpStatus.BAD_REQUEST,
                    exchange,
                    problemObjects,
                    ex.getMessage())
                    .build();

            return handleException(factory, exchange, problem);
        }

        if (ex instanceof AnimeNaoEncontradoException) {
            exchange.getResponse().setStatusCode(HttpStatus.NOT_FOUND);
            Problem problem = ExceptionUtils.createProblem(HttpStatus.NOT_FOUND,
                    exchange,
                    ProblemMessages.RECURSO_NAO_ENCONTRADO.getMessage(),
                    ex.getMessage())
                    .build();

            return handleException(factory, exchange, problem);
        }

        log.info(exchange.getResponse().getStatusCode().toString()); // printa "200 OK", não capturando o código proveniente da exceção

        // Extraindo httpStatusCode da Exception
        HttpStatus httpStatus = ExceptionUtils.getHttpStatusFromExceptionMessage(ex.getMessage());
        // Problem Default
        Problem problem = ExceptionUtils.createProblem(
                httpStatus,
                exchange,
                ProblemMessages.MSG_ERRO_GENERICA.getMessage(),
                ex.getMessage())
                .build();
        exchange.getResponse().setStatusCode(httpStatus);

        return handleException(factory, exchange, problem);
    }


    private Mono<Void> handleException(DataBufferFactory factory, ServerWebExchange exchange, Serializable value)
            throws JsonProcessingException {

        try {
            var buffer = factory.wrap(objectMapper.writeValueAsBytes(value));
            return exchange.getResponse().writeWith(just(buffer));

        } catch (Exception e) {
            Problem problemException = ExceptionUtils.createProblem(HttpStatus.INTERNAL_SERVER_ERROR, exchange,
                    ProblemMessages.MSG_ERRO_GENERICA.getMessage(), e.getMessage())
                    .build();

            var exceptionBuffer = factory.wrap(
                    objectMapper.writeValueAsBytes(problemException)
            );
            return exchange.getResponse().writeWith(just(exceptionBuffer));
        }
    }

}