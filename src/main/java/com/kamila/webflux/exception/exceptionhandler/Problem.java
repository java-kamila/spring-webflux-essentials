package com.kamila.webflux.exception.exceptionhandler;

import lombok.*;

import java.io.Serializable;
import java.time.OffsetDateTime;

/**
 * Modelo de representação de objeto de resposta em caso de exceção/erro.
 */
@Getter
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Problem implements Serializable {

    // "2021-08-06T18:03:35.571+30:00"
    private OffsetDateTime timestamp;

    // /anime
    private String path;

    // 400
    private int status;

    // Bad Request
    private String error;

    // Dados inválidos.
    private String message;

    // name: The name of this anime cannot be empty
    private String description;

}
