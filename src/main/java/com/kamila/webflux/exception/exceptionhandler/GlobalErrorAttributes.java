package com.kamila.webflux.exception.exceptionhandler;

import com.kamila.webflux.exception.AnimeNaoEncontradoException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.reactive.error.DefaultErrorAttributes;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.web.reactive.function.server.ServerRequest;

import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Component
public class GlobalErrorAttributes extends DefaultErrorAttributes {

    @Autowired
    private MessageSource messageSource;

    @Override
    public Map<String, Object> getErrorAttributes(ServerRequest request, ErrorAttributeOptions options) {
        log.info(">> GlobalErrorAttributes <<");

        request.exchange().getResponse().getHeaders().setContentType(MediaType.APPLICATION_JSON);

        Map<String, Object> errorAttributesMap = super.getErrorAttributes(request, options);

        Throwable ex = getError(request);
        log.error("[EXCEPTION]: " + ex.getMessage());

        if (ex instanceof WebExchangeBindException) {
            log.info(">> Global - WebExchangeBindException");

            String problemObjects = (String) ((WebExchangeBindException) ex).getAllErrors().stream()
                    .map(this::getObjectErrorMessages)
                    .collect(Collectors.joining("."));

            request.exchange().getResponse().setStatusCode(HttpStatus.BAD_REQUEST);
            insertErrorAttributes(errorAttributesMap,
                    HttpStatus.BAD_REQUEST,
                    problemObjects,
                    ex.getMessage()
            );
            return errorAttributesMap;
        }

        if (ex instanceof AnimeNaoEncontradoException) {
            log.info(">> Global - AnimeNaoEncontradoException capturado <<");
            request.exchange().getResponse().setStatusCode(HttpStatus.NOT_FOUND);
            insertErrorAttributes(errorAttributesMap,
                    HttpStatus.NOT_FOUND,
                    ProblemMessages.RECURSO_NAO_ENCONTRADO.getMessage(),
                    ex.getMessage()
            );
            return errorAttributesMap;
        }

        // Response Default
        HttpStatus status = HttpStatus.valueOf((int) errorAttributesMap.get("status"));
        insertErrorAttributes(
                errorAttributesMap,
                status,
                ProblemMessages.MSG_ERRO_GENERICA.getMessage(),
                ex.getMessage()
        );
        return errorAttributesMap;
    }

    private String getObjectErrorMessages(ObjectError objectError) {
        String message = messageSource.getMessage(objectError, LocaleContextHolder.getLocale());
        String name = objectError.getObjectName();

        if (objectError instanceof FieldError) {
            name = ((FieldError) objectError).getField();
        }
        return name + ": " + message;
    }

    private void insertErrorAttributes(Map<String, Object> errorAttributes, HttpStatus httpStatus,
                                       String message, String description) {

        errorAttributes.remove("requestId"); // Is not useful
        errorAttributes.put("status", httpStatus.value());
        errorAttributes.put("message", message);
        errorAttributes.put("description", description);
        errorAttributes.put("error", httpStatus.getReasonPhrase());
    }

}
