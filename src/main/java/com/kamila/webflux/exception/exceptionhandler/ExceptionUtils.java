package com.kamila.webflux.exception.exceptionhandler;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ServerWebExchange;

import java.time.OffsetDateTime;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExceptionUtils {

    public static Problem.ProblemBuilder createProblem(HttpStatus status, ServerWebExchange exchange, String message, String description) {
        return Problem.builder()
                .status(status.value())
                .error(status.getReasonPhrase())
                .timestamp(OffsetDateTime.now())
                .message(message)
                .path(exchange.getRequest().getPath().value())
                .description(description);
    }

    public static HttpStatus getHttpStatusFromExceptionMessage(String exceptionMsg) {
        int code = getErrorCode(exceptionMsg);
        if (code != -1) {
            return HttpStatus.valueOf(code);
        }
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }

    public static int getErrorCode(final String exceptionMsg) {
        String pattern = "-?\\d+";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(exceptionMsg);
        if (m.find()) {
            return Integer.parseInt(m.group(0));
        }
        return -1;
    }

}
