package com.kamila.webflux.exception.exceptionhandler;

import lombok.Getter;

@Getter
public enum ProblemMessages {

    MSG_ERRO_GENERICA("Falha na solicitação, tente novamente mais tarde."),
    RECURSO_NAO_ENCONTRADO("Recurso não encontrado."),
    DADOS_INVALIDOS("Dados inválidos.");

    private String message;

    ProblemMessages(String message) {
        this.message = message;
    }

}
