package com.kamila.webflux.exception;

public class AnimeNaoEncontradoException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public AnimeNaoEncontradoException(String message) {
        super(message);
    }

    public AnimeNaoEncontradoException(Long idAnime) {
        this(String.format("Não existe cadastro de Anime com código %d", idAnime));
    }

}
