package com.kamila.webflux.domain;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@With
@Table("tb_anime")
public class Anime {

	@Id
	private Long id;

	@NotBlank(message = "The name of this anime cannot be empty")
	private String name;
	
}
