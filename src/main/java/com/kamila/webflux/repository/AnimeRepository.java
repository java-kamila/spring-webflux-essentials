package com.kamila.webflux.repository;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import com.kamila.webflux.domain.Anime;

import reactor.core.publisher.Mono;

public interface AnimeRepository extends ReactiveCrudRepository<Anime, Long>{

	Mono<Anime> findById(Long id);
	
}
