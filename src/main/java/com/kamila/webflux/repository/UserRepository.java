package com.kamila.webflux.repository;

import com.kamila.webflux.domain.User;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

public interface UserRepository extends ReactiveCrudRepository<User, Long>{

	Mono<User> findByUsername(String username);
	
}
